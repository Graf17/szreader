#include <ESP8266WiFi.h>
#include <PubSubClient.h>

//Config

const int requestPin = 5;           //To request pin (pin 2) on P1 Port
const int serialBaudRate = 115200;  //Baudrate of Smartmeter
const int sendInterval = 5;         //How often you want an update (in seconds)
const int waitTime = 1100;          //Max time to wait for message, after request pin is high (in millis)
const int messageLength = 121;      //Number of bytes per message
const byte firstByte = 0x7E;        //First byte
const byte lastByte = 0x7E;         //Last byte

//WiFi and MQTT settings

const char* ssid = "ssid";
const char* pwd = "pwd";
const char* mqttServerIp = "ip";
const char* mqttClientName = "szreaderDataTransmitter";
const char* mqttUsername = "";
const char* mqttPwd = "";
const int mqttServerPort = 1883;

const char* willTopic = "szreader/transmitteroff";
const int willQos = 0;
const int willRetain = 0;
const char* willMessage = "SZReader Data Transmitter unexpectedly disconnected!";

const char* sessionIdTopic = "szreader/transmitterSessionId";
const boolean sessionIdRetain = true;
const char* messageTopic = "szreader/rawMessage";
const boolean messageRetain = false;

//End of config

WiFiClient espClient;
PubSubClient client(espClient);

unsigned long sessionId;
unsigned long sendMillis;

byte message[messageLength];
String wholeMessage;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  pinMode(requestPin, OUTPUT);
  digitalWrite(requestPin, LOW);

  sendMillis = millis();
  
  reconnectWifi();

  client.setServer(mqttServerIp, mqttServerPort);
  reconnectMQTT();
  sessionId = random(2147483647);
  client.publish(sessionIdTopic, String(sessionId).c_str(), sessionIdRetain);

  Serial.begin(serialBaudRate);
}

void loop() {
  if(!client.connected()){
    reconnectWifi();
    reconnectMQTT();
  }

  if(millis() - sendMillis >= sendInterval*1000){
    while(Serial.available() > 0){
      byte trash = Serial.read();
    }
    
    receiveMessage();

    if(message[0] == firstByte && message[messageLength-1] == lastByte){
      if(client.connected() && WiFi.status() == WL_CONNECTED){
        client.publish(messageTopic, wholeMessage.c_str(), messageRetain);
      }else{
        reconnectWifi();
        reconnectMQTT();
        client.publish(messageTopic, wholeMessage.c_str(), messageRetain);
      }
    }
  }

  while(Serial.available() > 0){
    byte trash = Serial.read();
  }

  if(client.connected() && WiFi.status() == WL_CONNECTED){
    client.loop();
  }else{
    reconnectWifi();
    reconnectMQTT();
  }
}

void receiveMessage(){
  sendMillis = millis();

  digitalWrite(requestPin, HIGH);

  unsigned long requestMillis = millis();
  while(Serial.available() < messageLength && millis()-requestMillis <= waitTime){}

  digitalWrite(requestPin, LOW);
  
  for(int i = 0; i<messageLength; i++){
    message[i] = Serial.read();
  }

  if(message[0]<16) wholeMessage = "0" + String(message[0], HEX);
  else wholeMessage = String(message[0], HEX);

  for(int i = 1; i<messageLength; i++){
    if(message[i]<16) wholeMessage = wholeMessage + " 0" + String(message[i], HEX);
    else wholeMessage = wholeMessage + " " + String(message[i], HEX);
  }
}

void reconnectMQTT(){
  digitalWrite(LED_BUILTIN, LOW);
  while(!client.connected()){
    if(client.connect(mqttClientName, mqttUsername, mqttPwd, willTopic, willQos, willRetain, willMessage)){
      digitalWrite(LED_BUILTIN, HIGH);
    }else{
      delay(50);
    }
  }
}

void reconnectWifi(){
  digitalWrite(LED_BUILTIN, LOW);
  WiFi.disconnect();
  WiFi.mode(WIFI_OFF);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pwd);
  while(WiFi.status() != WL_CONNECTED){
    delay(50);
  }
  digitalWrite(LED_BUILTIN, HIGH);
}

