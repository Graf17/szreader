import logging
import re
from xml.dom.minidom import parseString, Node

import dateutil.parser
import pytz
from tzlocal import get_localzone

from management.PublishingData import PublishingData

logger = logging.getLogger(__name__)


class MessageMangler:
    def __init__(self, sendername: str, raw_message):
        """ Tries to parse fields for PublishingData-objects from raw XML string
        Args:
            sendername: Name of the datasource that sent the data
            raw_message (str): Raw decoded data to parse for fields into PublishingData
        """
        self._raw_message = raw_message
        self._publishing_data = PublishingData(sourcename=sendername)
        self._timestamp_ok = False
        self._current_counter_in_ok = False
        self._current_counter_out_ok = False
        self._current_power_in_ok = False
        self._current_power_out_ok = False
        self._local_tz = get_localzone()
        self._prepare_data()

    def _prepare_data(self):
        logger.debug("Parsing XML data...")
        msg = re.sub(r'(<!--Decrypt)(.*)', "", self._raw_message)
        try:
            root = parseString(msg)
        except:
            logger.exception("Error parsing XML")
            return

        logger.debug("Locating fields in parsed XML...")
        data_notification = root.getElementsByTagName("DataNotification")
        if data_notification and len(data_notification) > 0:
            for node in data_notification[0].childNodes:
                if node.nodeType == Node.COMMENT_NODE:
                    # First Comment on the datanotification node contains the date and time
                    logger.debug("Found datetime comment field: {}".format(node.data))
                    dt = dateutil.parser.parse(node.data)
                    local_dt = dt.replace(tzinfo=pytz.utc).astimezone(self._local_tz)
                    logger.debug("With TZ info: {}".format(repr(local_dt)))
                    self._publishing_data.timestamp = int(local_dt.timestamp())
                    self._timestamp_ok = True
                    break
        value_nodes = data_notification[0].getElementsByTagName("UInt32")
        if len(value_nodes) > 2:
            # First 2 UInt32 under NotificationBody/DataValue/Structure are counter reading in and out. Last 2 UInt32 Nodes are power in and out in hex
            logger.debug("Found {} UInt32 nodes...".format(len(value_nodes)))

            c_out = value_nodes[-5].getAttribute("Value")
            logger.debug("CounterOut: {}".format(c_out))
            c_in = value_nodes[-6].getAttribute("Value")
            logger.debug("CounterIn: {}".format(c_in))

            p_out = value_nodes[-1].getAttribute("Value")
            logger.debug("PowerOut: {}".format(p_out))
            p_in = value_nodes[-2].getAttribute("Value")
            logger.debug("PowerIn: {}".format(p_in))

            self._publishing_data.current_counter_out = int("0x" + c_out, 0)
            self._current_counter_out_ok = True
            self._publishing_data.current_counter_in = int("0x" + c_in, 0)
            self._current_counter_in_ok = True

            self._publishing_data.current_power_out = int("0x" + p_out, 0)
            self._current_power_out_ok = True
            self._publishing_data.current_power_in = int("0x" + p_in, 0)
            self._current_power_in_ok = True

    def is_data_valid(self) -> bool:
        return self._current_counter_in_ok and self._current_counter_out_ok and self._current_power_in_ok and self._current_power_out_ok and self._timestamp_ok

    def get_publishing_data(self) -> PublishingData:
        return self._publishing_data
