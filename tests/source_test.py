from unittest import TestCase

from datasink.HttpDataSink import HttpDataSink
from datasink.MqttDataSink import MqttDataSink
from datasink.TextDataSink import TextDataSink
from datasource.MqttDataSource import MqttDataSource
from datasource.TextFileDataSource import TextFileDataSource
from management.Configuration import Configuration
from management.Factory import Factory
from datasource.SerialDataSource import SerialDataSource
from tests import TEST_CONFIG


class Source(TestCase):
    def setUp(self) -> None:
        self.configuration = Configuration(TEST_CONFIG)
        self.factory = Factory(self.configuration)

    def test_init_source_serial(self):
        serial = self.factory.create_source("serial", None)
        self.assertIsInstance(serial, SerialDataSource)

    def test_init_source_text(self):
        text = self.factory.create_source("textin", None)
        self.assertIsInstance(text, TextFileDataSource)

    def test_init_source_mqtt_in(self):
        mqtt_in = self.factory.create_source("mqttin", None)
        self.assertIsInstance(mqtt_in, MqttDataSource)

    def test_init_sink_http(self):
        http = self.factory.create_sink("http")
        self.assertIsInstance(http, HttpDataSink)

    def test_init_sink_mqtt_out(self):
        mqtt_out = self.factory.create_sink("mqttout")
        self.assertIsInstance(mqtt_out, MqttDataSink)

    def test_init_sink_text(self):
        text = self.factory.create_sink("textout")
        self.assertIsInstance(text, TextDataSink)