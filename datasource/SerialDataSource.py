import serial
import logging
import signal

TIMEOUT = 5
SERIAL_TIMED_OUT = False
from datasource import PACKET_DELIMITER
from datasource.IDataSource import IDataSource

logger = logging.getLogger(__name__)


def serial_timeout(signum, frame):
    logger.error("No valid data received from serial connection within {} seconds, aborting.".format(TIMEOUT))
    global SERIAL_TIMED_OUT
    SERIAL_TIMED_OUT = True


signal.signal(signal.SIGALRM, serial_timeout)


class SerialDataSource(IDataSource):

    def __init__(self, configuration, callback):
        """
        Args:
            configuration: Configuration instance holding data for serial connection
            callback: Method called with data received from serial connection
        """
        super().__init__(configuration, callback)
        self._ser = None
        logger.info("Initialized SerialDataSource {}".format(self.configurationname))

    def _start_source(self):
        logger.debug("Starting serial connection...")
        self._ser = serial.Serial(
            port=self._configuration.get('port', '/dev/ttyUSB0'),
            baudrate=int(self._configuration.get('baudrate', 115200)),
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1
        )
        logger.debug("Started serial connection")

        while not self.shutdown_flag.isSet():
            # Reading serial Interface and waiting for a Start-Packet ("7E" in DLMS)
            signal.alarm(TIMEOUT)
            global SERIAL_TIMED_OUT
            while self._ser.read().hex() != PACKET_DELIMITER:
                if SERIAL_TIMED_OUT:
                    self.shutdown_flag.set()
                    break
                pass
            if SERIAL_TIMED_OUT:
                break
            signal.alarm(0)
            # Adding start of packet to the message to be decrypted
            _message = PACKET_DELIMITER
            _buffer = bytearray(1)

            # Reading one packet by one waiting for a End-Packet (again "7E" in DLMS)
            while self._ser.readinto(_buffer) == 1 and format(_buffer[0], 'x').zfill(2) != PACKET_DELIMITER:
                _message += format(_buffer[0], 'x').zfill(2)
            _message += PACKET_DELIMITER

            # The resulting Message can only be decrypted if it consists of at least 200 characters.
            # Shorter Messages will be ignored
            if len(_message) >= 200:
                logger.info("Got data: {}".format(_message.strip()))
                # Notify Controller of received data via callback (if any)
                if self._callback is not None:
                    self._callback(self, _message)
        self._stop_source()

    def _stop_source(self):
        logger.info("Stopping execution of SerialDataSource {}...".format(self.configurationname))
        if self._ser.is_open:
            self._ser.close()
        logger.info("Stopped SerialDataSource {}".format(self.configurationname))
