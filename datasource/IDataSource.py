from abc import ABCMeta, abstractmethod
from threading import Thread, Event


class IDataSource(Thread):
    __meta__ = ABCMeta

    def __init__(self, configuration, callback):
        Thread.__init__(self)
        # The shutdown_flag is a threading.Event object that
        # indicates whether the thread should be terminated.
        self.shutdown_flag = Event()
        self._configuration = configuration
        self._callback = callback
        self._decryptionkey = self._configuration.get("decryptionkey", "").strip()

    def run(self) -> None:
        self._start_source()

    def terminate(self):
        self._stop_source()

    @property
    def decryptionkey(self):
        return self._decryptionkey

    @property
    def configurationname(self):
        return self._configuration.name

    @abstractmethod
    def _start_source(self): raise NotImplementedError

    @abstractmethod
    def _stop_source(self): raise NotImplementedError
