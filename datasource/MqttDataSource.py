from datasource.IDataSource import IDataSource
import logging
import paho.mqtt.client as mqtt

logger = logging.getLogger(__name__)


class MqttDataSource(IDataSource):

    def __init__(self, configuration, callback):
        super().__init__(configuration, callback)
        self._host = self._configuration.get("host")
        self._port = int(self._configuration.get("port", 1883))
        self._qos = int(self._configuration.get("qos", 1))
        self._topic = self._configuration.get("topic")
        self._wtopic = self._configuration.get("wtopic")
        self._wpayload = self._configuration.get("wpayload")
        self._wqos = int(self._configuration.get("wqos"))
        self._wretain = int(self._configuration.get("wretain"))
        self._client_name = self._configuration.get("clientname", "szreader_" + self.configurationname)
        try:
            self._client = mqtt.Client(self._client_name)
            self._client.username_pw_set(username=self._configuration.get("username", None),
                                         password=self._configuration.get("password", None))
            self._client.will_set(self._wtopic, payload=self._wpayload, qos=self._wqos, retain=self._wretain)
            self._client.enable_logger(logger=logger)
            self._client.message_callback_add(self._topic, self._message_callback)
            self._client.on_connect = self._connect_callback
            self._client.on_message = self._message_callback
            self._client.on_disconnect = self._disconnect_callback
            self._client.connect(host=self._host, port=self._port)
        except:
            logger.exception("Error in MQTT connection")

    def _connect_callback(self, client, userdata, flags, rc):
        if rc == 0:
            logger.info("Connected to MQTT Server {} on port {}".format(self._host, self._port))
            self._client.subscribe(self._topic, self._qos)
            logger.debug("Subscribed to topic {} for messages".format(self._topic))
        else:
            logger.error("Could not connect to MQTT server, RC: {}".format(rc))

    def _message_callback(self, client, userdata, message):
        pl = None
        try:
            pl = message.payload.decode('utf-8')
            logger.info("Got data on topic {}: {}".format(message.topic, pl))
        except:
            logger.warn("Error decoding message payload")
        if not pl is None:
            self._callback(self, pl)

    def _disconnect_callback(self, client, userdata, rc):
        if not self.shutdown_flag.isSet():
            logger.debug("Received disconnect event, reconnecting...")
            self._client.reconnect()
            logger.debug("Reconnected client {} to server {}".format(self._client_name, self._host))

    def _start_source(self):
        logger.debug("Starting source {}".format(self.configurationname))
        while not self.shutdown_flag.isSet():
            self._client.loop()
        self._stop_source()

    def _stop_source(self):
        logger.info("Stopping MqttDataSource {}...".format(self.configurationname))
        self._client.disconnect()
        logger.info("Stopped MqttDataSource {}".format(self.configurationname))
