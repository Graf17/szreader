from datasource.IDataSource import IDataSource
import time
import logging

logger = logging.getLogger(__name__)


class TextFileDataSource(IDataSource):

    def __init__(self, configuration, callback):
        super().__init__(configuration, callback)
        self._in_file = self._configuration.get("inputFile")
        self._interval = int(self._configuration.get("interval", 1))
        self._break = False
        logger.info("Initialized TextFileDataSource {} with file \"{}\" and interval {}s".format(self.configurationname, self._in_file, self._interval))

    def _start_source(self):
        self._file_handle = open(self._in_file)

        if self._file_handle.readable() and self._callback is not None:
            line = self._file_handle.readline()
            while line and not self.shutdown_flag.isSet():
                logger.info("Got data: {}".format(line.strip()))
                self._callback(self, line.strip())
                time.sleep(self._interval)
                line = self._file_handle.readline()
            self._stop_source()

    def _stop_source(self):
        logger.info("Stopping TextFileDataSource {}...".format(self.configurationname))
        if self._file_handle and not self._file_handle.closed:
            self._file_handle.close()
        logger.info("Stopped TextFileDataSource {}".format(self.configurationname))
